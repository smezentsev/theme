<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'deesoft/yii2-angular' => 
  array (
    'name' => 'deesoft/yii2-angular',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dee/angular' => $vendorDir . '/deesoft/yii2-angular',
    ),
    'bootstrap' => 'dee\\angular\\Bootstrap',
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'antkaz/yii2-ajax' => 
  array (
    'name' => 'antkaz/yii2-ajax',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@antkaz/ajax' => $vendorDir . '/antkaz/yii2-ajax/src',
    ),
  ),
  'loveorigami/yii2-modal-ajax' => 
  array (
    'name' => 'loveorigami/yii2-modal-ajax',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@lo/widgets/modal' => $vendorDir . '/loveorigami/yii2-modal-ajax/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui/src',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'devanych/yii2-cart' => 
  array (
    'name' => 'devanych/yii2-cart',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@devanych/cart' => $vendorDir . '/devanych/yii2-cart',
    ),
  ),
  'kartik-v/yii2-tree-manager' => 
  array (
    'name' => 'kartik-v/yii2-tree-manager',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/tree' => $vendorDir . '/kartik-v/yii2-tree-manager/src',
    ),
  ),
  'creocoder/yii2-nested-sets' => 
  array (
    'name' => 'creocoder/yii2-nested-sets',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@creocoder/nestedsets' => $vendorDir . '/creocoder/yii2-nested-sets/src',
    ),
  ),
  'kartik-v/yii2-checkbox-x' => 
  array (
    'name' => 'kartik-v/yii2-checkbox-x',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/checkbox' => $vendorDir . '/kartik-v/yii2-checkbox-x/src',
    ),
  ),
  'lavrentiev/yii2-toastr' => 
  array (
    'name' => 'lavrentiev/yii2-toastr',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@lavrentiev/widgets/toastr' => $vendorDir . '/lavrentiev/yii2-toastr/src',
    ),
  ),
  'halumein/yii2-wishlist' => 
  array (
    'name' => 'halumein/yii2-wishlist',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@halumein/wishlist' => $vendorDir . '/halumein/yii2-wishlist',
    ),
  ),
  'wapmorgan/yii2-inflection' => 
  array (
    'name' => 'wapmorgan/yii2-inflection',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@wapmorgan/yii2inflection' => $vendorDir . '/wapmorgan/yii2-inflection/src',
    ),
  ),
  'thyseus/yii2-favorites' => 
  array (
    'name' => 'thyseus/yii2-favorites',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@thyseus/favorites' => $vendorDir . '/thyseus/yii2-favorites',
    ),
  ),
  'mamadali/yii2-favorites' => 
  array (
    'name' => 'mamadali/yii2-favorites',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@mamadali/favorites' => $vendorDir . '/mamadali/yii2-favorites',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop/src',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
  '2amigos/yii2-leaflet-extension' => 
  array (
    'name' => '2amigos/yii2-leaflet-extension',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/leaflet' => $vendorDir . '/2amigos/yii2-leaflet-extension/src',
    ),
  ),
);
