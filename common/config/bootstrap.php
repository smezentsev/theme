<?php

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@userImages', dirname(dirname(__DIR__)) . '/frontend/web/images/users');
Yii::setAlias('@productImages', dirname(dirname(__DIR__)) . '/frontend/web/images/products');
Yii::setAlias('@newsImages', dirname(dirname(__DIR__)) . '/frontend/web/images/news');
Yii::setAlias('@categoryImages', dirname(dirname(__DIR__)) . '/frontend/web/images/categories');
//
//Yii::setAlias('@siteurl', 'http://admin' );
Yii::setAlias('@image', 'http://theme' );
//
//Yii::setAlias('@common_images', dirname( dirname( __DIR__ ) ) . '/images' );
//
//Yii::setAlias( '@cimages', '@siteurl/frontend/web/images' );
