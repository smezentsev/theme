<div class="card widget widget-categories">
  <div class="widget__header">
    <h4>Категории</h4>
  </div>
  <ul class="widget-categories__list widget-categories__list--root" data-collapse data-collapse-opened-class="widget-categories__item--open">
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Инструменты и гараж
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Фары и освещение
      </a>
      <button class="widget-categories__expander" type="button" data-collapse-trigger></button>
      <div class="widget-categories__container" data-collapse-content>
        <ul class="widget-categories__list widget-categories__list--child">
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Фары</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Задние фонари</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Противотуманные огни</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Поворотники</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Переключатели и реле</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Угловые светильники</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Детали интерьера
      </a>
      <button class="widget-categories__expander" type="button" data-collapse-trigger></button>
      <div class="widget-categories__container" data-collapse-content>
        <ul class="widget-categories__list widget-categories__list--child">
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Коврики</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Манометры</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Консоли и Органайзеры</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Мобильная электроника</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Рулевые колеса</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Грузовые аксессуары</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Двигатель и трансмиссия
      </a>
      <button class="widget-categories__expander" type="button" data-collapse-trigger></button>
      <div class="widget-categories__container" data-collapse-content>
        <ul class="widget-categories__list widget-categories__list--child">
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Воздушные фильтры</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Датчики кислорода</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Обогрев</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Выхлоп</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Шатуны и поршни</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Руководства по ремонту
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Подвеска
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Кузовные детали
      </a>
      <button class="widget-categories__expander" type="button" data-collapse-trigger></button>
      <div class="widget-categories__container" data-collapse-content>
        <ul class="widget-categories__list widget-categories__list--child">
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Капюшоны</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Решетки</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Противотуманные огни</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Противотуманные огни</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Дверная ручка</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Автомобильные чехлы</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Задние ворота</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Топливные системы
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Тормоза и подвеска
      </a>
      <button class="widget-categories__expander" type="button" data-collapse-trigger></button>
      <div class="widget-categories__container" data-collapse-content>
        <ul class="widget-categories__list widget-categories__list--child">
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Тормозные диски</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Ступицы колеса</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Пневматическая подвеска</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Шаровые шарниры</a>
          </li>
          <li class="widget-categories__item">
            <a href="" class="widget-categories__link">Комплекты тормозных колодок</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Рулевое управление
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Коробка передач
      </a>
    </li>
    <li class="widget-categories__item" data-collapse-item>
      <a href="" class="widget-categories__link">
        Воздушные фильтры
      </a>
    </li>
  </ul>
</div>
