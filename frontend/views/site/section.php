<?php
use app\components\SliderWidget;
use app\components\ItemsWidget;
use app\components\TrendingItemsWidget;

?>

<div class="section-content">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-12 col-12 main-left">
                <div class="col-1"></div>
                <div id="shopify-section-left-banner1"
                     class="shopify-section home-section">
                    <div class="widget_multibanner radius_3">
                        <div class="container-full">
                            <div class="widget-content">
                                <div class="row">
                                    <div
                                        class="item_banner item1 col-md-12 col-sm-12 col-xxs-12 col-xs-12">
                                        <div class="banners">
                                            <div class="">
                                                <a href="/collections/furniture" title="ss-emarket2"> <img
                                                        class="img-responsive lazyautosizes lazyloaded"
                                                        data-sizes="auto"
                                                        src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner4.png?v=1525839495"
                                                        alt="ss-emarket2"
                                                        data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner4.png?v=1525839495"
                                                        sizes="40px">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-product-carousel"
                     class="shopify-section home-section">
                    <div
                        class="widget_bestseler left-product-carousel owl-style_dot">
                        <div class="home-title">
                            <h2>BESTSELLER</h2>
                        </div>
                        <div class="ss-carousel ss-owl banner-carousel">
                            <div class="owl-carousel owl-loaded owl-drag"
                                 data-nav="false" data-dots="true" data-margin="0"
                                 data-autoplay="false" data-autospeed="3333"
                                 data-speed="3333" data-column1="1" data-column2="1"
                                 data-column3="2" data-column4="2" data-column5="2">
                                <div class="owl-stage-outer">
                                    <div class="owl-stage"
                                         style="width: 500px; transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s;">
                                        <div class="owl-item active" style="width: 250px;">
                                            <div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/capicola-brisket"
                                                           class="product-img"> <img
                                                                class="lazyautosizes lazyloaded" data-sizes="auto"
                                                                src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/42_6a728b1a-e67d-4f8f-919f-70ddf28b5517_80x80.jpg?v=1524712739"
                                                                data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/42_6a728b1a-e67d-4f8f-919f-70ddf28b5517_80x80.jpg?v=1524712739"
                                                                alt="Labor occaecat bee" sizes="80px">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/capicola-brisket"
                                                                   title="Labor occaecat bee" class="product-name">Labor
                                                                    occaecat bee</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665869320258"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$34.00">$34.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$42.00">$42.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/aliquip-sintfugia"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/47_2eb5cd23-6b89-4df6-99b4-aa29a608afbf_80x80.jpg?v=1524718937"
                                                                                  alt="Short ribs shank pork">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/aliquip-sintfugia"
                                                                   title="Short ribs shank pork" class="product-name">Short
                                                                    ribs shank pork</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665870860354"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$126.00">$126.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$154.00">$154.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/capicola-drumstic"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/40_80x80.jpg?v=1524712075"
                                                                                  alt="Irure rump cow bris">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/capicola-drumstic"
                                                                   title="Irure rump cow bris" class="product-name">Irure
                                                                    rump cow bris</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665868468290"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$234.00">$234.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$245.00">$245.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/brisket-voluptab"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/48_c49f710d-79e0-4e57-b5be-88a60f214f58_80x80.jpg?v=1524708894"
                                                                                  alt="Capicola leber ham">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/brisket-voluptab"
                                                                   title="Capicola leber ham" class="product-name">Capicola
                                                                    leber ham</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665865125954"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$334.00">$334.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$337.00">$337.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 250px;">
                                            <div>
                                                <div class="item">
                                                    <div class="product-item clearfix ">
                                                        <a href="/products/bresaola-volupta"
                                                           class="product-img"> <img
                                                                class="lazyautosizes lazyloaded" data-sizes="auto"
                                                                src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/38_10b49095-1c0e-4b9c-a382-cc63001c822d_80x80.jpg?v=1524708739"
                                                                data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/38_10b49095-1c0e-4b9c-a382-cc63001c822d_80x80.jpg?v=1524708739"
                                                                alt="Boudin ando bualo" sizes="80px">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/bresaola-volupta"
                                                                   title="Boudin ando bualo" class="product-name">Boudin
                                                                    ando bualo</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665864798274"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="price-new"><span class="money"
                                                                                              data-currency-usd="$37.00">$37.00</span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/aoccaecat-pariatur"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/56_154d1cb4-2679-4f1b-885e-d71895941e39_80x80.jpg?v=1524714591"
                                                                                  alt="Rank chicken bresao">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/aoccaecat-pariatur"
                                                                   title="Rank chicken bresao" class="product-name">Rank
                                                                    chicken bresao</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665870172226"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$472.00">$472.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$478.00">$478.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/capicola-bacon" class="product-img">
                                                            <img class="lazyload" data-sizes="auto"
                                                                 src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                 data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/43_80x80.jpg?v=1524710764"
                                                                 alt="Ham hock qui molit">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/capicola-bacon"
                                                                   title="Ham hock qui molit" class="product-name">Ham
                                                                    hock qui molit</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665867354178"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$412.00">$412.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$460.00">$460.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/broident-mollit"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/55_5a80e58d-4195-499f-9389-69edc319ea38_80x80.jpg?v=1524721555"
                                                                                  alt="Vupim ball tip flank">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/broident-mollit"
                                                                   title="Vupim ball tip flank" class="product-name">Vupim
                                                                    ball tip flank</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665871843394"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$51.00">$51.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$59.00">$59.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-nav disabled">
                                    <div class="owl-prev">
                                        <i class="fa fa-angle-left"></i>
                                    </div>
                                    <div class="owl-next">
                                        <i class="fa fa-angle-right"></i>
                                    </div>
                                </div>
                                <div class="owl-dots">
                                    <div class="owl-dot active">
                                        <span></span>
                                    </div>
                                    <div class="owl-dot">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-policy"
                     class="shopify-section home-section">
                    <div class="widget-services policy_left">
                        <div class="bg-policy">
                            <h3 class="title-home hidden">false</h3>
                            <div class="inner">
                                <div class="policy policy1">
                                    <div class="icon-policy">
                                        <i class="fa fa-file-text-o"></i>
                                    </div>
                                    <div class="info">
                                        <a href="#" class="title">free delivery</a>
                                        <p class="des">On order over $49.86</p>
                                    </div>
                                </div>
                                <div class="policy policy1">
                                    <div class="icon-policy">
                                        <i class="fa fa-shield"></i>
                                    </div>
                                    <div class="info">
                                        <a href="#" class="title">Order protecttion</a>
                                        <p class="des">Secured information</p>
                                    </div>
                                </div>
                                <div class="policy policy1">
                                    <div class="icon-policy">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <div class="info">
                                        <a href="#" class="title">Promotion gift</a>
                                        <p class="des">Special offers!</p>
                                    </div>
                                </div>
                                <div class="policy policy1">
                                    <div class="icon-policy">
                                        <i class="fa fa-money"></i>
                                    </div>
                                    <div class="info">
                                        <a href="#" class="title">money back</a>
                                        <p class="des">Return over 30 days</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-banner2"
                     class="shopify-section home-section">
                    <div class="widget_multibanner radius_3">
                        <div class="container-full">
                            <div class="widget-content">
                                <div class="row">
                                    <div
                                        class="item_banner item1 col-md-12 col-sm-12 col-xxs-12 col-xs-12">
                                        <div class="banners">
                                            <div class="">
                                                <a href="/products/bresaola-volupta" title="ss-emarket2">
                                                    <img class="img-responsive lazyload" data-sizes="auto"
                                                         src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                         alt="ss-emarket2"
                                                         data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner5.png?v=1525841016">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-blogs"
                     class="shopify-section home-section">
                    <div class="widget-blog-slide owl-style_dot radius_3">
                        <div class="container_full">
                            <div class="widget-blogs">
                                <div class="home-title">
                                    <h2>Lastest Blog</h2>
                                </div>
                                <div class="widget-content">
                                    <div class="ss-carousel ss-owl">
                                        <div class="owl-carousel owl-loaded owl-drag"
                                             data-nav="false" data-dots="true" data-margin="15"
                                             data-autoplay="true" data-autospeed="10000"
                                             data-speed="300" data-column1="1" data-column2="1"
                                             data-column3="3" data-column4="2" data-column5="2">
                                            <div class="owl-stage-outer">
                                                <div class="owl-stage"
                                                     style="width: 795px; transform: translate3d(-530px, 0px, 0px); transition: all 0.25s ease 0s;">
                                                    <div class="owl-item"
                                                         style="width: 250px; margin-right: 15px;">
                                                        <div class="blog-item blog-item-1">
                                                            <div class="blog-image">
                                                                <a href="/blogs/news/heard-of-shopify"> <img
                                                                        class="img-responsive"
                                                                        src="//cdn.shopify.com/s/files/1/0017/0770/4386/articles/19_480x.jpg?v=1524738647"
                                                                        alt="Heard of Shopify">
                                                                </a>
                                                            </div>
                                                            <div class="blog-detail">
                                                                <div class="item-right">
                                                                    <h3 class="blog-title text-truncate">
                                                                        <a href="/blogs/news/heard-of-shopify">Heard of
                                                                            Shopify</a>
                                                                    </h3>
                                                                    <div class="article_info">
                                                                        <span class="article_info__date"><i
                                                                                class="fa fa-calendar"></i> Apr 26, 2018</span> <span
                                                                            class="article_info__author"><span><i
                                                                                    class="fa fa-user-secret" aria-hidden="true"></i>
                                                                                By</span> Emarket Store</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="owl-item"
                                                         style="width: 250px; margin-right: 15px;">
                                                        <div class="blog-item blog-item-2">
                                                            <div class="blog-image">
                                                                <a href="/blogs/news/accumsan-lectus-consequat"> <img
                                                                        class="img-responsive"
                                                                        src="//cdn.shopify.com/s/files/1/0017/0770/4386/articles/17_480x.jpg?v=1524564037"
                                                                        alt="Accumsan lectus consequat">
                                                                </a>
                                                            </div>
                                                            <div class="blog-detail">
                                                                <div class="item-right">
                                                                    <h3 class="blog-title text-truncate">
                                                                        <a href="/blogs/news/accumsan-lectus-consequat">Accumsan
                                                                            lectus consequat</a>
                                                                    </h3>
                                                                    <div class="article_info">
                                                                        <span class="article_info__date"><i
                                                                                class="fa fa-calendar"></i> Apr 24, 2018</span> <span
                                                                            class="article_info__author"><span><i
                                                                                    class="fa fa-user-secret" aria-hidden="true"></i>
                                                                                By</span> Emarket Store</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="owl-item active"
                                                         style="width: 250px; margin-right: 15px;">
                                                        <div class="blog-item blog-item-3">
                                                            <div class="blog-image">
                                                                <a href="/blogs/news/nunc-fringilla-ipsum"> <img
                                                                        class="img-responsive"
                                                                        src="//cdn.shopify.com/s/files/1/0017/0770/4386/articles/2_480x.jpg?v=1524564013"
                                                                        alt="Nunc fringilla ipsum">
                                                                </a>
                                                            </div>
                                                            <div class="blog-detail">
                                                                <div class="item-right">
                                                                    <h3 class="blog-title text-truncate">
                                                                        <a href="/blogs/news/nunc-fringilla-ipsum">Nunc
                                                                            fringilla ipsum</a>
                                                                    </h3>
                                                                    <div class="article_info">
                                                                        <span class="article_info__date"><i
                                                                                class="fa fa-calendar"></i> Apr 24, 2018</span> <span
                                                                            class="article_info__author"><span><i
                                                                                    class="fa fa-user-secret" aria-hidden="true"></i>
                                                                                By</span> Emarket Store</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="owl-nav disabled">
                                                <div class="owl-prev">
                                                    <i class="fa fa-angle-left"></i>
                                                </div>
                                                <div class="owl-next">
                                                    <i class="fa fa-angle-right"></i>
                                                </div>
                                            </div>
                                            <div class="owl-dots">
                                                <div class="owl-dot">
                                                    <span></span>
                                                </div>
                                                <div class="owl-dot">
                                                    <span></span>
                                                </div>
                                                <div class="owl-dot active">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-product-carousel2"
                     class="shopify-section home-section">
                    <div
                        class="widget_bestseler left-product-carousel owl-style_dot">
                        <div class="home-title">
                            <h2>New arrivals</h2>
                        </div>
                        <div class="ss-carousel ss-owl banner-carousel">
                            <div class="owl-carousel owl-loaded owl-drag"
                                 data-nav="false" data-dots="true" data-margin="0"
                                 data-autoplay="false" data-autospeed="3333"
                                 data-speed="3333" data-column1="1" data-column2="1"
                                 data-column3="2" data-column4="2" data-column5="2">
                                <div class="owl-stage-outer">
                                    <div class="owl-stage"
                                         style="width: 500px; transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s;">
                                        <div class="owl-item active" style="width: 250px;">
                                            <div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/capicola-brisket"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/42_6a728b1a-e67d-4f8f-919f-70ddf28b5517_80x80.jpg?v=1524712739"
                                                                                  alt="Labor occaecat bee">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/capicola-brisket"
                                                                   title="Labor occaecat bee" class="product-name">Labor
                                                                    occaecat bee</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665869320258"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$34.00">$34.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$42.00">$42.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/consequat-burgdogis"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/22_2ec1ae0b-097d-406b-a59e-c79db22776dc_80x80.jpg?v=1524718003"
                                                                                  alt="Repre hende labor">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/consequat-burgdogis"
                                                                   title="Repre hende labor" class="product-name">Repre
                                                                    hende labor</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665870336066"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$31.00">$31.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$42.00">$42.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/aliquip-sintfugia"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/47_2eb5cd23-6b89-4df6-99b4-aa29a608afbf_80x80.jpg?v=1524718937"
                                                                                  alt="Short ribs shank pork">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/aliquip-sintfugia"
                                                                   title="Short ribs shank pork" class="product-name">Short
                                                                    ribs shank pork</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665870860354"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$126.00">$126.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$154.00">$154.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/jaeger-nobrisket"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/4_90781396-f998-4d40-8520-2180f0d8ca99_80x80.jpg?v=1524712263"
                                                                                  alt="Jaeger nobrisket">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/jaeger-nobrisket"
                                                                   title="Jaeger nobrisket" class="product-name">Jaeger
                                                                    nobrisket</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665868632130"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$200.00">$200.00</span></span> <span
                                                                    class="price-old"><span class="money"
                                                                                        data-currency-usd="$210.00">$210.00</span> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 250px;">
                                            <div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/capicola-drumstic"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/40_80x80.jpg?v=1524712075"
                                                                                  alt="Irure rump cow bris">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/capicola-drumstic"
                                                                   title="Irure rump cow bris" class="product-name">Irure
                                                                    rump cow bris</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665868468290"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$234.00">$234.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$245.00">$245.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/brisket-voluptab"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/48_c49f710d-79e0-4e57-b5be-88a60f214f58_80x80.jpg?v=1524708894"
                                                                                  alt="Capicola leber ham">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/brisket-voluptab"
                                                                   title="Capicola leber ham" class="product-name">Capicola
                                                                    leber ham</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665865125954"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$334.00">$334.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$337.00">$337.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix ">
                                                        <a href="/products/bresaola-volupta"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/38_10b49095-1c0e-4b9c-a382-cc63001c822d_80x80.jpg?v=1524708739"
                                                                                  alt="Boudin ando bualo">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/bresaola-volupta"
                                                                   title="Boudin ando bualo" class="product-name">Boudin
                                                                    ando bualo</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665864798274"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="price-new"><span class="money"
                                                                                              data-currency-usd="$37.00">$37.00</span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="product-item clearfix  on-sale">
                                                        <a href="/products/andjaeger-eiusmo"
                                                           class="product-img"> <img class="lazyload"
                                                                                  data-sizes="auto"
                                                                                  src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                                                  data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/products/57_8c55041b-0f37-4d7c-83ec-7d83460705f8_80x80.jpg?v=1524712602"
                                                                                  alt="Jowl tender pork">
                                                        </a>
                                                        <div class="product-info">
                                                            <div class="text-truncate">
                                                                <a href="/products/andjaeger-eiusmo"
                                                                   title="Jowl tender pork" class="product-name">Jowl
                                                                    tender pork</a>
                                                            </div>
                                                            <div class="custom-reviews a-left hidden-xs">
                                                                <span class="spr-badge" id="spr_badge_665869123650"
                                                                      data-rating="0.0"><span
                                                                        class="spr-starrating spr-badge-starrating"><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i><i
                                                                            class="spr-icon spr-icon-star-empty"></i></span><span
                                                                        class="spr-badge-caption">No reviews</span> </span>
                                                            </div>
                                                            <div class="price">
                                                                <!-- snippet/product-price.liquid -->
                                                                <span class="visually-hidden">Regular price</span> <span
                                                                    class="price-new"><span class="money"
                                                                                        data-currency-usd="$161.00">$161.00</span></span> <span
                                                                    class="price-old"> <span class="money"
                                                                                         data-currency-usd="$200.00">$200.00</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-nav disabled">
                                    <div class="owl-prev">
                                        <i class="fa fa-angle-left"></i>
                                    </div>
                                    <div class="owl-next">
                                        <i class="fa fa-angle-right"></i>
                                    </div>
                                </div>
                                <div class="owl-dots">
                                    <div class="owl-dot active">
                                        <span></span>
                                    </div>
                                    <div class="owl-dot">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-left-banner3"
                     class="shopify-section home-section">
                    <div class="widget_multibanner radius_3">
                        <div class="container-full">
                            <div class="widget-content">
                                <div class="row">
                                    <div
                                        class="item_banner item1 col-md-12 col-sm-12 col-xxs-12 col-xs-12">
                                        <div class="banners">
                                            <div class="">
                                                <a href="/products/andouille-ribeye" title="ss-emarket2">
                                                    <img class="img-responsive lazyload" data-sizes="auto"
                                                         src="//cdn.shopify.com/s/files/1/0017/0770/4386/t/3/assets/icon-loadings.svg?2"
                                                         alt="ss-emarket2"
                                                         data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner6.png?v=1525841020">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-10 col-lg-9 col-md-12 col-12 main-right">
                <?= SliderWidget::widget(['message' => 'Good morning']) ?>
                <div id="shopify-section-1525853944020"
                     class="shopify-section home-section">
                    <div class="widget_multibanner radius_3">
                        <div class="container-full">
                            <h3 class="widget-title hidden">Home Banner 1</h3>
                            <div class="widget-content">
                                <div class="row">
                                    <div
                                        class="item_banner item1 col-ld-12 col-md-12 col-sm-12 col-12">
                                        <div class="banners">
                                            <div class="">
                                                <a href="/collections/furniture" title="ss-emarket2"> <img
                                                        class="img-responsive lazyautosizes lazyloaded"
                                                        data-sizes="auto" src="images/3.png?v=1525853971"
                                                        alt="ss-emarket2" data-src="images/3.png?v=1525853971"
                                                        sizes="40px">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-1526095425683" class="shopify-section">
                    <div class="widget-product-tabs">
                        <div class="full-container">
                            <div class="widget-content products-listing grid">
                                <div class="ltabs-tabs-container">
                                    <div class="home-title">
                                        <span>Trending Items</span>
                                    </div>
                                    <div class="tabs-menu">
                                        <span class="tabs-menu_btn d-lg-none"><i
                                                class="fa fa-bars"></i></span>
                                        <ul class="tabs-menu_title tabs-title">
                                            <li class="tabs-menu-label current"
                                                data-url="/collections/furniture"
                                                data-tab="tab-1526095443235"><span>LivingRoom</span></li>
                                            <li class="tabs-menu-label"
                                                data-url="/collections/ellectronic"
                                                data-tab="tab-1526095621542"><span>Electronics</span></li>
                                            <li class="tabs-menu-label"
                                                data-url="/collections/women-s-clothing"
                                                data-tab="tab-1526101252378"><span>Fashions</span></li>
                                        </ul>
                                    </div>
                                    <div class="tabs-content product-layout">
                                        <div id="tab-1526095443235" class="tab-content current">
                                            <div class="ss-carousel ss-owl">
                                                <div class="owl-carousel owl-drag"
                                                     data-nav="true" data-margin="30" data-autoplay="false"
                                                     data-autospeed="10000" data-speed="300"
                                                     data-column1="5" data-column2="3" data-column3="3"
                                                     data-column4="2" data-column5="2">
                                                    <?php

//                                                    foreach ($products as $item) { ?>
<!--                                                    <div class="item ">-->
<!--                                                        --><?//= TrendingItemsWidget::widget(['product' => $item, 'images' => $item->getFiles()->all()]) ?>
<!--                                                    </div>-->
<!--                                                    --><?php //} ?>
                                                    <div class="owl-nav disabled">
                                                        <div class="owl-prev disabled">
                                                            <i class="fa fa-angle-left"></i>
                                                        </div>
                                                        <div class="owl-next disabled">
                                                            <i class="fa fa-angle-right"></i>
                                                        </div>
                                                    </div>
                                                    <div class="owl-dots disabled">
                                                        <div class="owl-dot active">
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-1525859554530"
                     class="shopify-section home-section">
                    <div class="widget-deals-carousel owl-style2">
                        <div class="container_full">
                            <div class="wrap-product">
                                <div class="home-title">
                                    <span>DAILY DEALS</span>
                                </div>
                                <div class="products-listing grid">
                                    <div class="product-layout block-content">
                                        <div class="ss-carousel ss-owl">
                                            <div class="owl-carousel owl-loaded owl-drag"
                                                 data-dots="true" data-nav="true" data-margin="30"
                                                 data-autoplay="false" data-autospeed="10000"
                                                 data-speed="300" data-column1="2" data-column2="1"
                                                 data-column3="3" data-column4="2" data-column5="2">
                                                <div class="owl-stage-outer">
                                                    <div class="owl-stage"
                                                         style="width: 2800px; transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s;">
                                                        <div class="owl-item active"
                                                             style="width: 670px; margin-right: 30px;">
                                                            <div class="item">
<?= ItemsWidget::widget(['message' => 'Good morning']) ?>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item active"
                                                             style="width: 670px; margin-right: 30px;">
                                                            <div class="item">
<?= ItemsWidget::widget(['message' => 'Good morning']) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="owl-nav">
                                                    <div class="owl-prev disabled">
                                                        <i class="fa fa-angle-left"></i>
                                                    </div>
                                                    <div class="owl-next">
                                                        <i class="fa fa-angle-right"></i>
                                                    </div>
                                                </div>
                                                <div class="owl-dots">
                                                    <div class="owl-dot active">
                                                        <span></span>
                                                    </div>
                                                    <div class="owl-dot">
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-1525853867205"
                     class="shopify-section  home-section">
                    <div class="widget_multibanner radius_3">
                        <div class="container-full">
                            <h3 class="widget-title hidden">Home Banner 2</h3>
                            <div class="widget-content">
                                <div class="row">
                                    <div
                                        class="item_banner item1 col-ld-12 col-md-12 col-sm-12 col-12">
                                        <div class="banners">
                                            <div class="">
                                                <a href="/collections/cables-connectors"
                                                   title="ss-emarket2"> <img
                                                        class="img-responsive lazyautosizes lazyloaded"
                                                        data-sizes="auto" src="images/4.png?v=1525853915"
                                                        alt="ss-emarket2" data-src="images/4.png?v=1525853915"
                                                        sizes="1369px">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shopify-section-1525853494383" class="shopify-section home-section">
                    <div class="home-banner-ct banners radius_3 d-none d-md-block">
                        <div class="containers">
                            <div class="row">
                                <div class="banner1 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="bn-1 banner-image">
                                        <a href="/collections/furniture" title="ss-emarket2">
                                            <img class="img-responsive lazyautosizes lazyloaded" data-sizes="auto" src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner8.png?v=1525853521" alt="ss-emarket2" data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner8.png?v=1525853521" sizes="322px">
                                        </a>
                                    </div>
                                    <div class="bn-2 banner-image">
                                        <a href="/collections/furniture" title="ss-emarket2">
                                            <img class="img-responsive lazyautosizes lazyloaded" data-sizes="auto" src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner9.png?v=1525853536" alt="ss-emarket2" data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner9.png?v=1525853536" sizes="322px">
                                        </a>
                                    </div>
                                </div>
                                <div class="banner2 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="banner-home banner-image">
                                        <a href="/collections/women-s-clothing" title="ss-emarket2">
                                            <img class="img-responsive lazyautosizes lazyloaded" data-sizes="auto" src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner10.png?v=1525853559" alt="ss-emarket2" data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner10.png?v=1525853559" sizes="647px">
                                        </a>
                                    </div>
                                </div>
                                <div class="banner3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="bn-1 banner-image">
                                        <a href="/collections/furniture" title="ss-emarket2">
                                            <img class="img-responsive lazyautosizes lazyloaded" data-sizes="auto" src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner11.png?v=1525853622" alt="ss-emarket2" data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner11.png?v=1525853622" sizes="322px">
                                        </a>
                                    </div>
                                    <div class="bn-2 banner-image">
                                        <a href="/collections/furniture" title="ss-emarket2">
                                            <img class="img-responsive lazyautosizes lazyloaded" data-sizes="auto" src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner12.png?v=1525853632" alt="ss-emarket2" data-src="//cdn.shopify.com/s/files/1/0017/0770/4386/files/banner12.png?v=1525853632" sizes="322px">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
