<?php

use yii\helpers\Url;

$this->title = 'О нас';
?>

<div class="block">
  <div class="container container--max--xl">
    <div class="faq__header">
      <h1 class="faq__header-title">О нас</h1>
    </div>
    <div class="card mb-4">
      <div class="card-body card-body--padding--2">
        <div class="row">
          <div class="col-12">
            <?= $content ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
