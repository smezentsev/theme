<div class="block-space block-space--layout--after-header"></div>
<div class="block">
  <div class="container container--max--xl">
    <div class="row">
      <div class="col-12 col-lg-3 d-flex">
        <?php include_once(Yii::getAlias('@frontend/views/account/_navigation.php')); ?>
      </div>
      <div class="col-12 col-lg-9 mt-4 mt-lg-0">
        <div class="addresses-list">
          <a href="#" class="addresses-list__item addresses-list__item--new">
            <div class="addresses-list__plus"></div>
            <div class="btn btn-secondary btn-sm">Добавить адрес</div>
          </a>
          <div class="addresses-list__divider"></div>
          <div class="addresses-list__item card address-card">
            <div class="address-card__badge tag-badge tag-badge--theme">Текущий профиль</div>
            <div class="address-card__body">
              <div class="address-card__name">Сергей</div>
              <div class="address-card__row">
                Россия<br>
                115302, Moscow<br>
                ул. Варшавская, 1
              </div>
              <div class="address-card__row">
                <div class="address-card__row-title">Телефон</div>
                <div class="address-card__row-content">+7 (495) 146-43-00</div>
              </div>
              <div class="address-card__row">
                <div class="address-card__row-title">Email</div>
                <div class="address-card__row-content">work@smezentsev.ru</div>
              </div>
              <div class="address-card__footer">
                <a href="#">Редактироват</a>&nbsp;&nbsp;
                <a href="#">Удалить</a>
              </div>
            </div>
          </div>
          <div class="addresses-list__divider"></div>
          <div class="addresses-list__item card address-card">
            <div class="address-card__body">
              <div class="address-card__name">Андрей</div>
              <div class="address-card__row">
                Россия<br>
                Питер<br>
                ул. Пушкина, 1
              </div>
              <div class="address-card__row">
                <div class="address-card__row-title">Телефон</div>
                <div class="address-card__row-content">+7 (495) 146-43-00</div>
              </div>
              <div class="address-card__row">
                <div class="address-card__row-title">Email</div>
                <div class="address-card__row-content">work@smezentsev.ru</div>
              </div>
              <div class="address-card__footer">
                <a href="#">Редактироват</a>&nbsp;&nbsp;
                <a href="#">Удалить</a>
              </div>
            </div>
          </div>
          <div class="addresses-list__divider"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="block-space block-space--layout--before-footer"></div>
