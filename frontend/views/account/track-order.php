<div class="block-space block-space--layout--after-header"></div>
<div class="block">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5 col-xxl-4">
        <div class="card ml-md-3 mr-md-3">
          <div class="card-body card-body--padding--2">
            <h1 class="card-title card-title--lg">Отслеживание товара</h1>
            <p class="mb-4">
              Введите ID заказа и почтовый адрес, который вы используете для авторизации
            </p>
            <form>
              <div class="form-group">
                <label for="track-order-id">ID заказа</label>
                <input id="track-order-id" type="text" class="form-control" placeholder="ID заказа">
              </div>
              <div class="form-group">
                <label for="track-email">Email</label>
                <input id="track-email" type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group pt-4 mb-1">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Отправить</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="block-space block-space--layout--before-footer"></div>
