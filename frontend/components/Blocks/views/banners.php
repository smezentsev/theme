<div class="block-banners block">
  <div class="container">
    <div class="block-banners__list">
      <a href="" class="block-banners__item block-banners__item--style--one">
        <span class="block-banners__item-image"><img src="/images/banners/banner1.jpg" alt=""></span>
        <span class="block-banners__item-image block-banners__item-image--blur"><img src="/images/banners/banner1.jpg" alt=""></span>
        <span class="block-banners__item-title">МОТОРНЫЕ МАСЛА</span>
        <span class="block-banners__item-details">
            Синтетическое моторное масло с бесплатной доставкой<br>
            Гарантированная жизнь без трения
        </span>
        <span class="block-banners__item-button btn btn-primary btn-sm">
            Подробнее
        </span>
      </a>
      <a href="" class="block-banners__item block-banners__item--style--two">
        <span class="block-banners__item-image"><img src="/images/banners/banner2.jpg" alt=""></span>
        <span class="block-banners__item-image block-banners__item-image--blur"><img src="/images/banners/banner2.jpg" alt=""></span>
        <span class="block-banners__item-title">Сэкономьте до $40</span>
        <span class="block-banners__item-details">
          Роскошная деталь интерьера для настоящих эстетов <br>
          Кожа, ткань, слоновая кость и многое другое.
        </span>
        <span class="block-banners__item-button btn btn-primary btn-sm">
            Подробнее
        </span>
      </a>
    </div>
  </div>
</div>
