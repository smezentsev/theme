<?php
Yii::setAlias('@common', dirname(dirname(__DIR__)) . '/common');
Yii::setAlias('@frontend', dirname(__DIR__));
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
